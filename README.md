# alfa-books-api

## Solução

Criar uma API para inserir, alterar, deletar e consultar os livros da livraria Alfa Books.

### Banco de dados
O Banco de dados deve ser o MySQL.

Schema do banco
```
CREATE  TABLE `books` (
  `id INT  AUTOINCREMENT ,
  `name` VARCHAR(100) NOT NULL ,
  `author` VARCHAR(150) NOT NULL,
  `created` DATE NOT NULL,
  `updated` DATE,
  `quantity` INT NOT NULL,
  PRIMARY KEY (`id`) 
);

```

## Endpoints

### GET

O endpoint deve retornar todos os livros mostrando apenas o nome, autor e quantidade.
```
GET api/v1/books
```

### GET

O endpoint deve retornar um único livro, passando o id como parametro.
```
GET api/v1/books/:id
```

### POST

O endpoint deve receber um payload com as informações do livro e cadastrar no sistema.
```
POST api/v1/book

payload:
{
    name: "1984",
    author: "George Orwell",
    quantity: 20
}
```

#### Obs: Ao cadastrar o livro o campo created deve receber a data do dia cadastrado.

### PUT

O endpoint deve receber um payload com informaçōes para alterar os dados do livro e receber como parametro o id.

```
PUT api/v1/book/:id

payload:
{
    name: "1984",
    author: "George Orwell",
    quantity: 50
}
```

#### Obs: Ao alterar o livro o campo updated deve receber a data do dia que foi alterado.

### DELETE

O endpoint deve receber o id do livro que será deletado como parametro.

```
DELETE /api/v1/book/:id
```
